/**
 * Created by Administrator on 2017/5/29.
 */
var webpack = require("webpack");
var htmlWebpackPlugin = require("html-webpack-plugin");
var postcss = require('postcss');
var px2rem = require('postcss-px2rem');

var ExtractTextPlugin  = require("extract-text-webpack-plugin");
//nodejs的里的一个全局变量,它指向的是我们项目的根目录,入口文件的位置
module.exports = {
    // devtool: "eval-source-map",
  //配置入口文件
    entry: {
        index: __dirname + '/app/main.js',
      //   index2:__dirname + '/app/main2.js',
      // index3:__dirname + '/app/main3.js',
    },

    output: {
        //需要打包后的文件 放置的位置
        path: __dirname + '/public',
        //打包后文件的名字
      filename: 'js/[name].js',
        // filename: 'js/[name]-[chunkhash].js',
      // publicPath: "http://www.xianianwang.com" //用来上线时候输出的前缀
    },

    devServer: {
        contentBase: "./public",//本地服务器所加载的页面所在的目录
        // stats:{
        //     colors: true,//终端中输出结果为彩色---没有颜色了
        // },
        // historyApiFallback: true,//不跳转
        inline: true,//实时刷新
        port: 8080,
        // hot:true
    },
    module: {
      loaders: [
            {
                test: /\.json$/,
                use: 'json-loader'
            },
        //处理页面中以ejs为结尾的文件
          {
            test:/\.ejs$/,
            use:'ejs-loader'
          },
        //处理页面中以html为结尾的文件,使用了这个插件,ejs中的变量将不再有效果
        {
          test:/\.html$/,
          use:'html-loader'
        },
          //下面这个插件会将页面中的变量解析为正常的html文件,如何要在页面中使用htmlwebpackplugin变量则需要注释这个插件
            // {
            //   test: /\.css$/,
            //   //从右向左翻译,css-loader必须在右边,不然没办法执行style-loader
            //   use:[
            //     'style-loader', 'css-loader',
            //     {
            //     loader: "postcss-loader",
            //     options:{
            //       plugins: (loader) => [
            //         require('autoprefixer')
            //         ],
            //         minimize:true
            //     }
            //   }]
            // },
        //处理图片
            {
                test:/\.(png|jpg|git|woff)/,
                use: [
                  //将图片输出到images的目录之下如何大小在8192之下则打包成base64,如果在这这之前刚保存到./images 之下
                  {
                    loader: "url-loader?limit=8192?name=[name].[hash:6].[ext]&publicPath=./&outputPath=images/"
                  },
                  //压缩图片的大小
                  {
                    loader: "image-webpack-loader"
                  }
                ]
            },
        //将es6的代码转为es5的代码
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                }
            },
        //将css独立出来
            { test: /\.css$/,
              loader: ExtractTextPlugin.extract({fallback :'style-loader',
                use:[
                {
                  loader : 'css-loader'
                },
                  //给css添加前缀
                {
                  loader: 'postcss-loader',
                  options: {
                    plugins: (loader) => [
                      require('autoprefixer')({
                        browsers:['last 5 versions']
                      }),
                    ],
                    minimize:true
                  }
                },
              ]}) }
        ],
    },
    plugins: [

        new ExtractTextPlugin({filename: 'css/[name].css'}),//样式压缩
        //创建一个新的页面在public 以app/index.html中的文件为依据
        //   生产环境下启用：压缩js代码
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        },
        mangle: false,

      }),
        // new webpack.ProvidePlugin({
        //     $: "jquery",
        //     jQuery: "jquery",
        //     "window.jQuery": "jquery"
        // }),
        new htmlWebpackPlugin({
          filename: 'index.html',
          template: "app/index.html",
          title:"webpack title",
          inject: 'head',
          date:new Date(),
            minify: {
                removeComments: true,    //移除HTML中的注释
                // collapseWhitespace: true,    //删除空白符与换行符
                conservativeCollapse: true,
                minifyJS: true //js也在一行
            },
            // hash: true,
            // chunks: ['index'],
            // xhtml: true,
            // showErrors: true
        }),
        new htmlWebpackPlugin({
            filename: 'inset/aboutus.html',
            template: "app/inset/aboutus.html",
            title:"铁友轨道交通-联系我们页面",
            inject: 'body',
            minify: {
                removeComments: true,    //移除HTML中的注释
                // collapseWhitespace: true,    //删除空白符与换行符
                // conservativeCollapse: true,
                minifyJS: true //js也在一行
            },

            // hash: true,
            // chunks: ['index'],
            // xhtml: true,
            // showErrors: true
        }),
      // new htmlWebpackPlugin({
      //   filename: 'b.html',
      //   template: "app/index.html",
      //   title:"webpack b.html",
      //   inject: 'head',
      //   chunks:['index2']
      // }),
      // new htmlWebpackPlugin({
      //   filename: 'c.html',
      //   template: "app/index.html",
      //   title:"webpack c.html",
      //   inject: 'head',
      //   chunks:['index3']
      // }),
        // new webpack.HotModuleReplacementPlugin()
    ],
};
// var htmlWebpackPlugin=new htmlWebpackPlugin();